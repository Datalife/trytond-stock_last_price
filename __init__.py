# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from .stock import ShipmentOut, ShipmentIn, Move
from .sale import SaleLine


def register():
    Pool.register(
        SaleLine,
        module='stock_last_price', type_='model',
        depends=['sale'])
    Pool.register(
        Move,
        ShipmentIn,
        ShipmentOut,
        module='stock_last_price', type_='model')
